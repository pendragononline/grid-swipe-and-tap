$(document).ready(function () {

    var currentrow = 1; //stores the row number
    var currentcol = 1; //stores the column number 

    var currentcell = '#row' + currentrow.toString() + 'col' + currentcol.toString();

    printInfo();


    //Down
    $('#down-button').click(function (e) {
        currentrow++;
        location.href = '#row' + currentrow.toString() + 'col' + currentcol.toString();
        freezeDownwardsRow();
        printInfo();
    });

    //Right
    $('#right-button').click(function (e) {
        currentcol++;
        location.href = '#row' + currentrow.toString() + 'col' + currentcol.toString();
        freezeRightCol();
        printInfo();
    });

    //Left
    $('#left-button').click(function (e) {
        currentcol--;
        location.href = '#row' + currentrow.toString() + 'col' + currentcol.toString();
        freezeLeftCol();
        printInfo();
    });

    //Up
    $('#up-button').click(function (e) {
        currentrow--;
        location.href = '#row' + currentrow.toString() + 'col' + currentcol.toString();
        freezeUpwardsRow();
        printInfo();
    });

    //Print information to console
    function printInfo() {
        console.log('Row: ', currentrow, '| Column: ', currentcol, '| ID: #row' + currentrow + 'col' + currentcol, '| Coordinate: ', currentcol, ',', currentrow);
    }

    /*The functions below limit the value of the 'currentrow' and 'currentcol' variables to the number of rows and columns that are present 
    to stop people from clicking the buttons more times than is possible and then when they tap the button to move the grid in the opposite
    direction don't see anything after many clicks*/

    var rowcount = 3; //set for the number of rows in the grid
    var columncount = 4; //set for the number of columns in the grid

    $('.arrow-buttons').click(function () {
        hideArrows();
    });
   
    function freezeDownwardsRow() {
        if (currentrow > 3) { //set for number of rows in the grid
             currentrow = rowcount;
        }
    }

    function freezeUpwardsRow() {
        if (currentrow < 1) {
           currentrow = 1;
            }
        }

    function freezeRightCol() {
        if (currentcol > 3) { //set for number of rows in the grid
            currentcol = columncount;
        }
    }

    function freezeLeftCol() {
        if (currentrow < 1) { //set for number of rows in the grid
            currentcol = 1;
        }
    }

    /*Hide arrows based on cell ID -left is never hidden*/
    function hideArrows() {

        //All row 1
        if (currentrow === 1) {
            $('#up-button').css('display', 'none');
        }

        //Row1Col1, Col2, Col3
        else if (currentrow === 1 && currentcol <= 3) {
            $('#left-button, #down-button, #right-button').css('display', 'block');
            $('#up-button').css('display', 'none');
        }

        //Row1Col4
        else if (currentrow === 1 && currentcol === 4) {
            $('#left-button, #down-button').css('display', 'block');
            $('#right-button, #up-button').css('display', 'none');
        }

        //Row2Col1, Col2 & Col3
        else if (currentrow === 2 && currentcol <= 3) {
            $('.arrow-buttons').css('display', 'block');
        }

        //Row2Col4
        else if (currentrow === 2 && currentcol === 4) {
            $('#left-button, #down-button, #up-button').css('display', 'block');
            $('#right-button').css('display', 'none');
        }

        //Row3Col1, Col2, Col3
        else if (currentrow === 3 && currentcol <= 3) {
            $('#left-button, #up-button, #right-button').css('display', 'block');
            $('#down-button').css('display', 'none');
        }

        //Row3Col4
        else if (currentrow === 3 && currentcol === 4) {
            $('#left-button, #up-button').css('display', 'block');
            $('#right-button, #down-button').css('display', 'none');
        }
    }


    /*Keep buttons working when scrolling - the problem is that scrolling doesn't increase the value of 'currentrow' or 'currentcol',
    so each time the user scrolls the value of these needs to be incremented so that the buttons remain functional*/

    /*NOTE - WILL NOT HIDE ANY ARROWS*/

    window.addEventListener("scroll", scrollDown);

    function scrollDown() {

        //console.clear();
        var windowHeight = window.innerHeight;
        var pageLength = windowHeight * 3;
        var firstThird = pageLength / 3; //calculate each 'third' of the screen height by dividing the total screen height by 3, then *2 to get two thirds and adding *2 to one third to get three thirds
        var secondThird = firstThird * 2;
        var thirdThird = secondThird + firstThird;

        /*
        console.log("Window height: ", pageLength, "px");
        console.log("First third: ", firstThird, "px", " second third: ", secondThird, "px", " third third: ", thirdThird, "px");
        */

        var distanceFromTop = window.scrollY;
        //console.log("Distance from top is", distanceFromTop, "px");

        if (distanceFromTop <= firstThird -5 ) { //calculate the row by how far the page the user has scrolled/swiped - add a negative 5 pixel leeway
            currentrow = 1;
            //$('#up-button').addClass('hidden');
            //$('#down-button').removeClass('hidden');
        }

        else if (distanceFromTop > firstThird -5 && distanceFromTop < secondThird -5 ) {
            currentrow = 2;
            //$('#up-button, #down-button').removeClass('hidden');
        }

        else {
            currentrow = 3;
            //$('#up-button').removeClass('hidden');
            //$('#down-button').addClass('hidden');
        }

        //console.log("Row: ", currentrow);
    }

});



